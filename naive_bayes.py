import numpy as np, collections
from sklearn.feature_extraction.text import CountVectorizer 
from sklearn.linear_model import LogisticRegression

def get_data(file):
    with open(file) as f:
        contents = np.array([x.strip() for x in f.read().splitlines()])
    return contents

class NaiveBayesModel:
    features = None
    labels = None
    vocabulary = None

    def __init__(self, x_train, y_train, vocabulary):
        self.features = np.empty_like(x_train)
        self.labels = np.empty(len(y_train))
        self.vocabulary = vocabulary
        self.classify(x_train, y_train, True)

    # Get probability that the class label is 'label'
    def label_probablity(self, feature, label):
        # num_of_label = self.labels.count(label)
        num_of_label = collections.Counter(self.labels)[label]

        #Implementing Laplace Smoothing
        p_label = (num_of_label + 1) / (len(self.labels) + 2)

        # count_words = [0 for x in range(len(self.vocabulary))]
        count_words = np.zeros(len(self.vocabulary))

        #for the given label and feature, count the number of each of the words
        idx = self.features[self.labels == label] == feature
        for i in idx:
            count_words[i] += 1

        #Compute the probability of each word and find the product of all words and multiply with P(Y)
        return p_label * np.prod((count_words + 1) /  (num_of_label + 1))

    # Predict label after computing probability of both labels
    def classify(self, x, y, train=False):

        preds = []
        for i in range(len(x)):

            #For all lables find the probability and choose the class with highest probablity as teh prediction
            pred = 1 if (self.label_probablity(x[i], 0) < self.label_probablity(x[i], 1)) else 0

            #If we are training then we have to create the feature and labels that need to be used during testing
            if train:
                self.features[i] = x[i]
                self.labels[i] = y[i]

            preds.append(pred)

        return self.score(preds, y)

    #Compute the accuracy given the predictions and the actual lables
    def score(self, y_pred, y):
        return len([i for i, val in enumerate(y_pred) if val == y[i]])/len(y)

def main():
    print('Naive Bayes output: ')
    train = get_data('traindata.txt')
    y_train = np.array(list(map(int, get_data('trainlabels.txt'))))
    stop_words = get_data('stoplist.txt')

    #removing stop words
    x_train = np.array([' '.join([word for word in line.split() if word not in stop_words]) for line in train])
    #Create bag of words using count vectorizer. It creates an array with 1's and 0's corresponding to where
    #the word in the given inputs.
    vect = CountVectorizer()
    train_bag_of_words = vect.fit_transform(x_train).toarray()
    vocabulary = np.array(vect.get_feature_names())

    clr = NaiveBayesModel(train_bag_of_words, y_train, vocabulary)
    score_train = clr.classify(train_bag_of_words, y_train)
    print('Training accuracy is: ' + str(score_train))

    #repeat the above preprocessing for test data and compute accuracy
    test = get_data('testdata.txt')
    y_test = np.array(list(map(int, get_data('testlabels.txt'))))

    x_test = np.array([' '.join([word for word in line.split() if word not in stop_words]) for line in test])
    vect = CountVectorizer(vocabulary=vocabulary)
    test_bag_of_words = vect.fit_transform(x_test).toarray()

    score_test = clr.classify(test_bag_of_words, y_test)
    print('Test accuracy is: ' + str(score_test))

    #implementing logistic regression using the bag of words and the lables
    logisticRegr = LogisticRegression()
    logisticRegr.fit(train_bag_of_words, y_train)
    lr_train_accuracy = logisticRegr.score(train_bag_of_words, y_train)
    lr_test_accuracy = logisticRegr.score(test_bag_of_words, y_test)

    print('SK-learn training accuracy: ' + str(lr_train_accuracy))
    print('SK-learn test accuracy: ' + str(lr_test_accuracy))

if __name__ == "__main__": main()